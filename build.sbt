lazy val root = (project in file("."))
  .settings(
    inThisBuild(
      List(
        organization := "com.hivemind",
        scalaVersion := "2.12.7"
      )),
    name := "amazon-reviews",
    scalacOptions += "-Ypartial-unification",
    scalafmtOnCompile := true,
    libraryDependencies ++= Seq(
      library.akkaHttp,
      library.akkaHttpPlayJson,
      library.akkaStream,
      library.alpakkaFile,
      library.playJson,
      library.playJsonJoda,
      library.cats,
      library.akkaHttpTestKit % Test,
      library.akkaTestKit % Test,
      library.akkaStreamTestKit % Test,
      library.scalaTest % Test
    )
  )
  .settings(dockerSettings)
  .enablePlugins(JavaAppPackaging, DockerPlugin)

lazy val library = new {
  object Version {
    val akkaHttp = "10.1.5"
    val akkaHttpPlayJson = "1.22.0"
    val akka = "2.5.13"
    val alpakka = "0.20"
    val cats = "1.5.0"
    val playJson = "2.6.10"
    val scalaTest = "3.0.5"
  }

  val akkaHttp = "com.typesafe.akka" %% "akka-http" % Version.akkaHttp
  val akkaHttpPlayJson = "de.heikoseeberger" %% "akka-http-play-json" % Version.akkaHttpPlayJson
  val akkaStream = "com.typesafe.akka" %% "akka-stream" % Version.akka
  val alpakkaFile = "com.lightbend.akka" % "akka-stream-alpakka-file_2.12" % Version.alpakka
  val alpakkaElasticsearch = "com.lightbend.akka" %% "akka-stream-alpakka-elasticsearch" % Version.alpakka
  val playJson = "com.typesafe.play" %% "play-json" % Version.playJson
  val playJsonJoda = "com.typesafe.play" %% "play-json-joda" % Version.playJson
  val cats = "org.typelevel" %% "cats-core" % Version.cats
  val akkaHttpTestKit = "com.typesafe.akka" %% "akka-http-testkit" % Version.akkaHttp
  val akkaTestKit = "com.typesafe.akka" %% "akka-testkit" % Version.akka
  val akkaStreamTestKit = "com.typesafe.akka" %% "akka-stream-testkit" % Version.akka
  val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest
}

lazy val dockerSettings =
  Seq(
    daemonUser.in(Docker) := "root",
    maintainer.in(Docker) := "Josip Grgurica",
    version.in(Docker) := "latest",
    dockerBaseImage := "openjdk",
    dockerRepository := Some("jgrgurica")
  )
