# Amazon Reviews

Amazon is well known for its product review system.
Customers can write [reviews](https://www.amazon.com/Legend-Zelda-Links-Awakening-game-boy/dp/B00002ST3U?th=1#customerReviews) for products and rate them with stars.
Customer reviews themselves can also be rated as helpful or not by other customers.

What should you do if you want to find best rated products or if you want find influencers and their's reviews?
You should use **amazon-reviews** service!!! :smiley:

**amazon-reviews** aservice is data analysis service that exposes an API that accepts requests and provides analysis as its responses.


## Prerequisites
- Oracle JDK 8 or OpenJDK 8
- sbt >= 1.2.0
- docker >= 18.09.0
- docker-compose >= 1.23.2

## Usage

**amazon-reviews** relies on elasticsearch where it stores review data for faster searching and aggregations.
Besides elasticsearch **amazon-reviews** depends on review file that contains review json lines. 

Json file should be set using absolute path in `${JSON_FILE}` env variable.
```
export JSON_FILE=/.../resources/video_game_reviews_example.json
```

In production elasticsearch url should be exposed using `${ELASTICSERCH_URL}` env variable.
```
export ELASTICSERCH_URL=http://elasticsearch:9200
```

**amazon-reviews** comes with `docker-compose.yml` file. To run it locally, docker should be installed. It will connect **amazon-reviews** to elasticsearch and expose itself 
on 8080 port.
So if you want to run **amazon-reviews** navigate to `amazon-reviews` directory and run
```
sbt docker:publishLocal

docker-compose up
```
 and you will be able to access it on `localhost:8080`

### Find The Best Rated Products

If you want to search for the best rated products within a certain period of time you can use our **best-rated** API.
The rating of a product is determined by taking the average number of stars.
The number of results are at most the number specified by the `limit` field.
Additionally, only products with a minimum number of reviews are considered.
This is a tuning parameter that can be used to provide more meaningful results.

This api comes in two flavors, streamed and regular one.

#### Regular
Regular api is accessible using  `POST` requests to `{amazon-reviews-url}/amazon/best-rated` path with payload 

```json
{
  "start": "15.10.2011",
  "end": "01.08.2013",
  "limit": 2,
  "min_number_reviews": 2
}
```

This api returns at most 1000 products. To return more you should use stream api.

Response:
```json
[
    {
        "asin": "B0000482FN",
        "average_rating": 5
    },
    {
        "asin": "B0000488W1",
        "average_rating": 5
    },
    {
        "asin": "B00004C8S8",
        "average_rating": 5
    },
    {
        "asin": "B00004C8S9",
        "average_rating": 5
    }
]
```

Curl example:
```
curl -X POST \
  http://localhost:8080/amazon/best-rated \
  -H 'Content-Type: application/json' \
  -d '{
  "start": "15.10.2011",
  "end": "01.08.2013",
  "limit": 20,
  "min_number_reviews": 2
}'
```

#### Streamed
To access more than 1000 products you should use strem api. It similar to regular one.
It is is accessible using  `POST` requests to `{amazon-reviews-url}/amazon/best-rated/stream` path with payload 
```json
{
  "start": "15.10.2011",
  "end": "01.08.2013",
  "limit": 2,
  "min_number_reviews": 2
}
```

Response is stream of server send events and it looks like:
```
data:{"asin":"B00BDCNPCO","average_rating":4.666666666666667,"count":3}

data:{"asin":"B00BE6CUWK","average_rating":2.0833333333333335,"count":12}

data:{"asin":"B00BEJP3VC","average_rating":3.6666666666666665,"count":3}
```

Curl example
```
curl -X POST \
  http://localhost:8080/amazon/best-rated/stream \
  -H 'Content-Type: application/json' \
  -d '{
  "start": "15.10.2011",
  "end": "01.08.2013",
  "limit": 2,
  "min_number_reviews": 2
}'
```

### Finding Influential Reviews and Influential Users

If you are interested in identifying influential users you can use finding influential reviews api. It is streamed endpoint, similar to streamed best rated product.

It is accessible using `POST` requests to `localhost:8080/amazon/influencers/stream` path and a body like this:

```json
{
  "type": "influencer-reviews",
  "min_helpful_votes": 10,
  "helpful_percentage": 0.8,
  "search_phrases": ["love", "hate"]
}
```

Response:
```
data:{"reviewerId":"A2HM8BRW0GC1ZV","asin":"B003LQQGE4","reviewerName":"New York Mama","summary":"Baby Einstein vs Fisher Price Rainforest Jumperoo"}

data:{"reviewerId":"A2HM8BRW0GC1ZV","asin":"B000LXQVA4","reviewerName":"New York Mama","summary":"Rainforest Jumperoo vs. Baby Einstein Musical Motion Activity Jumper"}

data:{"reviewerId":"A2HM8BRW0GC1ZV","asin":"B0081ZOV06","reviewerName":"New York Mama","summary":"0-3 versus 3+"}

data:{"reviewerId":"A2ID5QQ1ZF9R2W","asin":"B000V98HCI","reviewerName":"Mommy of Cuties M & Z \"Mom of Twins\"","summary":"~*~*~THESE ARE NON-TOXIC * DON'T STINK & ARE TONS OF FUN~*~*~"}

data:{"reviewerId":"A2ID5QQ1ZF9R2W","asin":"B000V9IB0G","reviewerName":"Mommy of Cuties M & Z \"Mom of Twins\"","summary":"*~*~*~BEST FOAM MATS FOR BABIES/TODDLERS~EASY TO CLEAN~BETTER THAN KNEE PADS~MULTIPLE USES~DURABLE~*~*~*"}

data:{"reviewerId":"A2ID5QQ1ZF9R2W","asin":"B000AV5LZ8","reviewerName":"Mommy of Cuties M & Z \"Mom of Twins\"","summary":"IT WILL NOT CHIP YOUR TOILET SEAT...IT SITS ON THE BOWL!!!!!!"}
```

This api will return best 10 reviews for every influencer.


Curl example
```
curl -X POST  \
  http://localhost:8080/amazon/influencers/stream \
  -H 'Content-Type: application/json' \
  -d '{
  "type": "influencer-reviews",
  "min_helpful_votes": 8,
  "helpful_percentage": 0.8,
  "search_phrases": ["love", "hate"]
}'
```

## Limitations

#### Import
**amazon-reviews** serivce imports files from file path defined in `${JSON_FILE}` env variable. It imports full content of file and after that it listens for new
appends to files. It doesn't support deletion of file content and editing parts of file.

#### Api
Finding influential reviews and influential users api is limited in returing best 10 reviews for every influencer and might return duplicates.
Pagination is not currently supported.
