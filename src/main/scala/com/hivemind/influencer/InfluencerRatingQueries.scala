package com.hivemind.influencer

object InfluencerRatingQueries {

  val createIndex: String =
    s"""
       |{
       |    "mappings": {
       |        "influencer-rating": {
       |            "properties": {
       |                "reviewerId": {
       |                    "type": "keyword"
       |                },
       |                "asin": {
       |                    "type": "keyword"
       |                },
       |                "helpfulVotes": {
       |                    "type": "integer"
       |                },
       |                "helpfulPercentage": {
       |                    "type": "double"
       |                }
       |            }
       |        }
       |    }
       |}
       """.stripMargin

  def getInfluencerStream(helpfulVotes: Int,
                          helpfulPercentage: Double,
                          size: Int,
                          after: Option[String]): String = {
    val afterString =
      after.map(value => s""", "after": {"data": "$value"}""").getOrElse("")

    s"""
       |{
       |    "query": {
       |        "bool": {
       |            "filter": [
       |                {
       |                    "range": {
       |                        "helpfulVotes": {
       |                            "gte": $helpfulVotes
       |                        }
       |                    }
       |                },
       |                {
       |                    "range": {
       |                        "helpfulPercentage": {
       |                            "gte": $helpfulPercentage
       |                        }
       |                    }
       |                }
       |            ]
       |        }
       |    },
       |    "aggs": {
       |        "data": {
       |            "composite": {
       |            	"size": $size,
       |                "sources": [
       |                    {
       |                        "data": {
       |                            "terms": {
       |                                "field": "reviewerId"
       |                            }
       |                        }
       |                    }
       |                ]
       |                ${afterString}
       |            }
       |        }
       |    }
       |}
     """.stripMargin
  }
}
