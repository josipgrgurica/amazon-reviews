package com.hivemind.influencer

import akka.NotUsed
import akka.http.scaladsl.model.Uri.{Path, Query}
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import cats.data._
import cats.implicits._
import com.hivemind.configuration.Configuration
import com.hivemind.domain.{Influencer, InfluencerRating}
import com.hivemind.elastic.CompositeAggregationResult
import com.hivemind.http.HttpService
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport

import scala.concurrent.Future

class InfluencerRatingService(
    configuration: Configuration,
    httpService: HttpService)(implicit materializer: ActorMaterializer)
    extends PlayJsonSupport {
  private implicit val actorSystem = materializer.system
  private implicit val executionContext = materializer.system.dispatcher
  private val baseUri = Uri(configuration.elasticSearchUrl)

  def createIndex(): EitherT[Future, String, Unit] = {
    val payload = InfluencerRatingQueries.createIndex
    val path = Path("/" + InfluencerRating.index)

    EitherT {
      httpService.resourceExists(baseUri, path).flatMap {
        case true =>
          Future.successful(Right(()))
        case false =>
          for {
            response <- httpService
              .makeRequest(baseUri, HttpMethods.PUT, path, payload)
            discarded <- httpService.discardEntity(response)
          } yield discarded
      }
    }
  }

  def index(influencer: InfluencerRating): EitherT[Future, String, Unit] = {
    val documentId = s"${influencer.asin}:${influencer.reviewerId}"
    val path = Path("/" + InfluencerRating.index) / InfluencerRating.index / documentId
    val payload = InfluencerRating.jsonFormat.writes(influencer).toString()

    for {
      response <- EitherT.right[String](
        httpService.makeRequest(baseUri, HttpMethods.PUT, path, payload))
      _ <- EitherT(httpService.extractEntity(response))
    } yield ()
  }

  def influcencerSource(
      minHelpfulVotes: Int,
      helpfulPercentage: Double): Source[Influencer, NotUsed] = {
    sealed trait BatchState
    case object Starting extends BatchState
    case class Running(afterKey: String) extends BatchState
    case object Finished extends BatchState

    def getBatch(afterKey: Option[String])
      : Future[Option[(BatchState, List[Influencer])]] = {
      val path = Path("/" + InfluencerRating.index) / "_search"
      val payload = InfluencerRatingQueries.getInfluencerStream(
        minHelpfulVotes,
        helpfulPercentage,
        configuration.streamBatch,
        afterKey)
      val queryParams = Query("size" -> "0")

      val action = for {
        response <- EitherT.right[String](
          httpService
            .makeRequest(baseUri, HttpMethods.GET, path, payload, queryParams))
        entity <- EitherT(httpService.extractEntity(response))
        aggregation <- EitherT.right[String](
          Unmarshal(entity)
            .to[CompositeAggregationResult[Influencer]])
      } yield {
        aggregation
      }

      action.value.map {
        case Right(CompositeAggregationResult(data, Some(key))) =>
          Some(Running(key), data)
        case Right(CompositeAggregationResult(data, None)) =>
          Some(Finished, data)
        case Left(_) =>
          None
      }
    }

    Source
      .unfoldAsync[BatchState, List[Influencer]](Starting) {
        case Finished     => Future.successful(None)
        case Starting     => getBatch(None)
        case Running(key) => getBatch(Some(key))
      }
      .mapConcat(identity)
  }
}
