package com.hivemind.util

import akka.actor.Scheduler
import akka.pattern.after

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

object FutureUtils {
  def retry[T](f: => Future[T])(delay: FiniteDuration = 10.seconds,
                                retries: Int = 10)(
      implicit ec: ExecutionContext,
      s: Scheduler): Future[T] = {
    f recoverWith {
      case error if retries > 0 =>
        println(error)
        after(delay, s)(retry(f)(delay, retries - 1))
    }
  }
}
