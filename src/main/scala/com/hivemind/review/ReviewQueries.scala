package com.hivemind.review

object ReviewQueries {
  val createIndex =
    s"""
       |{
       |    "mappings": {
       |        "review": {
       |            "properties": {
       |                "reviewerId": {
       |                    "type": "keyword"
       |                },
       |                "asin": {
       |                    "type": "keyword"
       |                },
       |                "reviewerName": {
       |                    "type": "text"
       |                },
       |                "helpfulVotes": {
       |                    "type": "integer"
       |                },
       |                "totalVotes": {
       |                    "type": "integer"
       |                },
       |                "reviewText": {
       |                    "type": "text"
       |                },
       |                "overall": {
       |                    "type": "double"
       |                },
       |                "summary": {
       |                    "type": "text"
       |                },
       |                "unitReviewTime": {
       |                    "type": "long"
       |                },
       |                "reviewTime": {
       |                    "type": "date",
       |                    "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||MM dd, yyyy"
       |                }
       |            }
       |        }
       |    }
       |}
       """.stripMargin

  def searchByInfluencersAndReviewText(influencers: Set[String],
                                       word: String): String = {
    val influencersJson = influencers
      .map(influcencers => s""""$influcencers"""")
      .mkString("[", ",", "]")
    s"""
       |{
       |    "query": {
       |        "bool": {
       |            "must": [
       |                {
       |                    "fuzzy": {
       |                        "reviewText": "$word"
       |                    }
       |                }
       |            ],
       |            "filter": {
       |                "terms": {
       |                    "reviewerId": $influencersJson
       |                }
       |            }
       |        }
       |    }
       |}
     """.stripMargin
  }
}
