package com.hivemind.review

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri.Path
import akka.http.scaladsl.model.{HttpMethods, Uri}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import cats.data._
import cats.implicits._
import com.hivemind.command.GetInfluencerReviewsCommand
import com.hivemind.configuration.Configuration
import com.hivemind.domain.{InfluencerReview, Review}
import com.hivemind.elastic.HitResult
import com.hivemind.http.HttpService
import com.hivemind.influencer.InfluencerRatingService
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport

import scala.concurrent.Future

class ReviewService(configuration: Configuration,
                    influencerReviewService: InfluencerRatingService,
                    httpService: HttpService)(
    implicit val actorSystem: ActorSystem,
    implicit val materializer: ActorMaterializer)
    extends PlayJsonSupport {
  private implicit val executionContext = actorSystem.dispatcher
  private val baseUri = Uri(configuration.elasticSearchUrl)

  def createIndex(): EitherT[Future, String, Unit] = {
    val payload = ReviewQueries.createIndex
    val path = Path("/" + Review.index)

    EitherT {
      httpService.resourceExists(baseUri, path).flatMap {
        case true =>
          Future.successful(Right(()))
        case false =>
          for {
            response <- httpService
              .makeRequest(baseUri, HttpMethods.PUT, path, payload)
            discarded <- httpService.discardEntity(response)
          } yield discarded
      }
    }
  }

  def index(review: Review): EitherT[Future, String, Unit] = {
    val documentId = s"${review.asin}:${review.reviewerId}"
    val path = Path("/" + Review.index) / Review.index / documentId
    val payload = Review.writes.writes(review).toString()

    for {
      response <- EitherT.right[String](
        httpService.makeRequest(baseUri, HttpMethods.PUT, path, payload))
      _ <- EitherT(httpService.extractEntity(response))
    } yield ()
  }

  def getInfluencerReviewsSoruce(command: GetInfluencerReviewsCommand)
    : Source[InfluencerReview, NotUsed] = {
    val source = influencerReviewService
      .influcencerSource(command.min_helpful_votes, command.helpful_percentage)
      .mapAsync(1) { influencer =>
        Future
          .traverse(command.search_phrases)(phrase =>
            getReviews(Set(influencer.id), phrase).value)
          // sequence list of eithers to get either of list
          .map(_.sequence)
          .map {
            case Right(reviews) => reviews.flatten
            case Left(error) =>
              List()
          }
      }

    source.mapConcat(identity)
  }

  private def getReviews(influencers: Set[String], word: String) = {
    val path = Path("/" + Review.index) / "_search"
    val query =
      ReviewQueries.searchByInfluencersAndReviewText(influencers, word)

    for {
      response <- EitherT.right[String](
        httpService.makeRequest(baseUri, HttpMethods.GET, path, query))
      entity <- EitherT(httpService.extractEntity(response))
      hits <- EitherT.right[String](
        Unmarshal(entity).to[HitResult[InfluencerReview]])
    } yield hits.data
  }
}
