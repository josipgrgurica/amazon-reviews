package com.hivemind.importer

import java.nio.file.FileSystems

import akka.Done
import akka.stream.ActorMaterializer
import akka.stream.alpakka.file.scaladsl.FileTailSource
import akka.stream.scaladsl.Sink
import cats.data._
import cats.implicits._
import com.hivemind.configuration.Configuration
import com.hivemind.domain.{InfluencerRating, Review}
import com.hivemind.influencer.InfluencerRatingService
import com.hivemind.review.ReviewService
import play.api.libs.json.Json

import scala.concurrent.Future
import scala.concurrent.duration._

class ReviewImportService(configuration: Configuration,
                          reviewService: ReviewService,
                          influencerReviewService: InfluencerRatingService)(
    implicit val materializer: ActorMaterializer) {
  private implicit val executionContext = materializer.system.dispatcher

  private val dataPath = configuration.dataPath

  private val fs = FileSystems.getDefault

  def start(): Future[Done] = {
    FileTailSource
      .lines(
        path = fs.getPath(dataPath),
        maxLineSize = configuration.maxLineSize,
        pollingInterval = configuration.pollIntervalMillis.millis
      )
      .mapAsync(configuration.importParallelism) { line =>
        val action = for {
          review <- EitherT(
            Future.successful(
              Json.parse(line).validate[Review].asEither.leftMap(_.toString())))
          _ <- reviewService.index(review)
          _ <- if (review.helpfulVotes > 0)
            influencerReviewService.index(InfluencerRating.fromReview(review))
          else EitherT.rightT[Future, String](())
        } yield ()

        action.value.flatMap {
          case Right(value) => Future.successful(value)
          case Left(error)  => Future.failed(new RuntimeException(error))
        }
      }
      .runWith(Sink.ignore)
  }
}
