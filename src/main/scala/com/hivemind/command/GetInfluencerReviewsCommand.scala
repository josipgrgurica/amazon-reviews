package com.hivemind.command

import play.api.libs.json.{Json, Reads}

case class GetInfluencerReviewsCommand(
    `type`: String,
    min_helpful_votes: Int,
    helpful_percentage: Double,
    search_phrases: List[String]
)

object GetInfluencerReviewsCommand {
  implicit val jsonReads: Reads[GetInfluencerReviewsCommand] =
    Json.reads[GetInfluencerReviewsCommand]
}
