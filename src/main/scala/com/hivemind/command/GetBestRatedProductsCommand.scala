package com.hivemind.command

import org.joda.time.DateTime
import play.api.libs.json.{JodaReads, Json, Reads}

case class GetBestRatedProductsCommand(
    start: DateTime,
    end: DateTime,
    limit: Int,
    min_number_reviews: Int
)

object GetBestRatedProductsCommand {
  implicit val dateTimeReader: Reads[DateTime] =
    JodaReads.jodaDateReads("dd.MM.yyyy")

  implicit val reads: Reads[GetBestRatedProductsCommand] =
    Json.reads[GetBestRatedProductsCommand]
}
