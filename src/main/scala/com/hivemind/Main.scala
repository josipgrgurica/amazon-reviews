package com.hivemind

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import cats.data.EitherT
import cats.implicits._
import com.hivemind.configuration.Configuration
import com.hivemind.http.HttpService
import com.hivemind.importer.ReviewImportService
import com.hivemind.influencer.InfluencerRatingService
import com.hivemind.product.ProductService
import com.hivemind.review.ReviewService
import com.hivemind.server.Server
import com.hivemind.util.FutureUtils
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

object Main extends App {

  implicit val system: ActorSystem = ActorSystem("amazon-reviews")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContext = system.dispatcher

  implicit val scheduler = system.scheduler

  val configuration = new Configuration(ConfigFactory.load())
  val httpService = new HttpService()
  val influencerRatingService =
    new InfluencerRatingService(configuration, httpService)
  val reviewService =
    new ReviewService(configuration, influencerRatingService, httpService)
  val importService = new ReviewImportService(configuration,
                                              reviewService,
                                              influencerRatingService)
  val productService = new ProductService(configuration, httpService)
  val server = new Server(productService, reviewService)

  FutureUtils
    .retry(
      (for {
        _ <- reviewService.createIndex()
        _ <- influencerRatingService.createIndex()
        _ <- EitherT.right[String](importService.start())
      } yield ()).value
        .map {
          case Right(()) =>
            ()
          case Left(error) =>
            throw new RuntimeException(error)
        })(10.seconds, 10)

  server.start()

  Await.result(system.whenTerminated, Duration.Inf)
}
