package com.hivemind.http

import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Uri.{Path, Query}
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.util.ByteString

import scala.concurrent.Future

class HttpService(implicit materializer: ActorMaterializer) {
  private implicit val system = materializer.system
  private implicit val executionContext = materializer.system.dispatcher

  private val http = Http()

  def makeRequest(baseUri: Uri,
                  method: HttpMethod,
                  path: Path,
                  payload: String,
                  query: Query = Query()): Future[HttpResponse] =
    http
      .singleRequest(
        HttpRequest(method)
          .withUri(
            baseUri
              .withPath(path)
              .withQuery(query))
          .withEntity(
            HttpEntity(ContentTypes.`application/json`, ByteString(payload))
          )
      )

  def extractEntity(
      response: HttpResponse): Future[Either[String, ResponseEntity]] =
    if (response.status.isSuccess())
      Future.successful(Right(response.entity))
    else
      Unmarshal(response.entity)
        .to[String]
        .map(error => Left(error))

  def discardEntity(response: HttpResponse): Future[Either[String, Unit]] =
    if (response.status.isSuccess()) {
      response.entity.discardBytes()
      Future.successful(Right(()))
    } else
      Unmarshal(response.entity)
        .to[String]
        .map(error => Left(error))

  def resourceExists(baseUri: Uri, path: Path): Future[Boolean] =
    for {
      response <- http.singleRequest(
        HttpRequest(HttpMethods.HEAD).withUri(baseUri.withPath(path)))
      result <- discardEntity(response)
    } yield {
      result.isRight
    }
}
