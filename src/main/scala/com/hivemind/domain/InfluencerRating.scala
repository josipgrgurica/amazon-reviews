package com.hivemind.domain

import play.api.libs.json.{Format, Json}

case class InfluencerRating(
    asin: String,
    reviewerId: String,
    helpfulVotes: Int,
    helpfulPercentage: Double
)

object InfluencerRating {
  val index = "influencer-rating"

  def fromReview(review: Review): InfluencerRating =
    InfluencerRating(
      asin = review.asin,
      reviewerId = review.reviewerId,
      helpfulVotes = review.helpfulVotes,
      helpfulPercentage = review.helpfulVotes / (review.totalVotes * 1.0)
    )

  implicit val jsonFormat: Format[InfluencerRating] =
    Json.format[InfluencerRating]
}
