package com.hivemind.domain

import play.api.libs.json.Json

case class InfluencerReview(
    reviewerId: String,
    asin: String,
    reviewerName: Option[String],
    summary: String
)

object InfluencerReview {
  implicit val format = Json.format[InfluencerReview]
}
