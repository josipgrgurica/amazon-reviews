package com.hivemind.domain
import org.joda.time.DateTime
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json._

case class Review(
    reviewerId: String,
    asin: String,
    reviewerName: Option[String],
    helpfulVotes: Int,
    totalVotes: Int,
    reviewText: String,
    overall: Double,
    summary: String,
    unixReviewTime: Long,
    reviewTime: DateTime
)

object Review {
  val index = "review"

  implicit val jodaReads: Reads[DateTime] =
    JodaReads.jodaDateReads("MM dd, yyyy")
  implicit val jodaWrites: Writes[DateTime] =
    JodaWrites.jodaDateWrites("yyyy-MM-dd")

  implicit val writes: Writes[Review] = Json.writes[Review]
  implicit val reads: Reads[Review] = (
    (JsPath \ "reviewerID").read[String] and
      (JsPath \ "asin").read[String] and
      (JsPath \ "reviewerName").readNullable[String] and
      (JsPath \ "helpful")
        .read[List[Int]](minLength[List[Int]](2))
        .map(list => list.head) and
      (JsPath \ "helpful")
        .read[List[Int]](minLength[List[Int]](2))
        .map(list => list(1)) and
      (JsPath \ "reviewText").read[String] and
      (JsPath \ "overall").read[Double](min(1.0) keepAnd max(5.0)) and
      (JsPath \ "summary").read[String] and
      (JsPath \ "unixReviewTime").read[Long] and
      (JsPath \ "reviewTime").read[DateTime]
  )(Review.apply _)
}
