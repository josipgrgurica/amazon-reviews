package com.hivemind.domain

import play.api.libs.json.Reads._
import play.api.libs.json._

case class Influencer(id: String)

object Influencer {

  implicit val jsonReads: Reads[Influencer] =
    (JsPath \ "key" \ "data").read[String].map(Influencer(_))

  implicit val listReads: Reads[List[Influencer]] =
    (JsPath \ "hits" \ "hits").read[List[Influencer]]
}
