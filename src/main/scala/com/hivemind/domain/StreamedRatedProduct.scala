package com.hivemind.domain

import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json._

case class StreamedRatedProduct(
    asin: String,
    average_rating: Double,
    count: Int
)

object StreamedRatedProduct {
  implicit val jsonWrites: Writes[StreamedRatedProduct] =
    Json.writes[StreamedRatedProduct]
  implicit val jsonReads: Reads[StreamedRatedProduct] =
    (
      (JsPath \ "key" \ "data").read[String] and
        (JsPath \ "avg_overall" \ "value").read[Double] and
        (JsPath \ "doc_count").read[Int]
    )(StreamedRatedProduct.apply _)
}
