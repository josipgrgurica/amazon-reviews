package com.hivemind.domain

import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json._

case class RatedProduct(
    asin: String,
    average_rating: Double
)

object RatedProduct {
  implicit val jsonWrites: Writes[RatedProduct] = Json.writes[RatedProduct]
  implicit val jsonReads: Reads[RatedProduct] =
    (
      (JsPath \ "key").read[String] and
        (JsPath \ "avg_overall" \ "value").read[Double]
    )(RatedProduct.apply _)
}
