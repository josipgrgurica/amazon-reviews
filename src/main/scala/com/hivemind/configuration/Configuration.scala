package com.hivemind.configuration

import com.typesafe.config.Config

class Configuration(conf: Config) {
  val dataPath = conf.getString("dataPath")
  val elasticSearchUrl = conf.getString("elasticSearchUrl")
  val maxResponseLength = conf.getInt("maxResponseLength")
  val streamBatch = conf.getInt("streamBatch")
  val maxLineSize = conf.getInt("maxLineSize")
  val pollIntervalMillis = conf.getInt("pollIntervalMillis")
  val importParallelism = conf.getInt("importParallelism")
  val reviewSearchParallelism = conf.getInt("reviewSearchParallelism")
}
