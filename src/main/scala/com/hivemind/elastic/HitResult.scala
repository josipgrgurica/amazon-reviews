package com.hivemind.elastic

import play.api.libs.json.{JsPath, Reads}

case class HitResult[T](
    data: List[T]
)

case class Source[T](source: T)

object HitResult {
  implicit def sourceReads[T](implicit reads: Reads[T]): Reads[Source[T]] =
    (JsPath \ "_source").read[T].map(Source(_))

  implicit def hitReads[T](implicit reads: Reads[T]): Reads[HitResult[T]] =
    (JsPath \ "hits" \ "hits")
      .read[List[Source[T]]]
      .map(data => HitResult(data.map(_.source)))
}
