package com.hivemind.elastic

import play.api.libs.json.Reads._
import play.api.libs.json._

case class BucketAggregationResult[T](data: List[T])

object BucketAggregationResult {
  implicit def agregationReads[T](
      implicit reads: Reads[T]): Reads[BucketAggregationResult[T]] =
    (JsPath \ "aggregations" \ "data" \ "buckets")
      .read[List[T]]
      .map(BucketAggregationResult(_))
}
