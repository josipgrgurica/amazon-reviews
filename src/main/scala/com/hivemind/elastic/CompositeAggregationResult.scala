package com.hivemind.elastic

import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json._

case class CompositeAggregationResult[T](data: List[T],
                                         afterKey: Option[String])

object CompositeAggregationResult {
  implicit def agregationReads[T](
      implicit reads: Reads[T]): Reads[CompositeAggregationResult[T]] =
    (
      (JsPath \ "aggregations" \ "data" \ "buckets").read[List[T]] and
        (JsPath \ "aggregations" \ "data" \ "after_key" \ "data")
          .readNullable[String]
    )(CompositeAggregationResult.apply[T](_, _))
}
