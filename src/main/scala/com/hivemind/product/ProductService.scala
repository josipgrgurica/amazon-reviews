package com.hivemind.product

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri.{Path, Query}
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import cats.data.EitherT
import cats.implicits._
import com.hivemind.command.GetBestRatedProductsCommand
import com.hivemind.configuration.Configuration
import com.hivemind.domain.{RatedProduct, Review, StreamedRatedProduct}
import com.hivemind.elastic.{
  BucketAggregationResult,
  CompositeAggregationResult
}
import com.hivemind.http.HttpService
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport

import scala.concurrent.Future

class ProductService(configuration: Configuration, httpService: HttpService)(
    implicit val actorSystem: ActorSystem,
    implicit val materializer: ActorMaterializer)
    extends PlayJsonSupport {
  private implicit val executionContext = actorSystem.dispatcher
  private val baseUri = Uri(configuration.elasticSearchUrl)

  def getBestRated(command: GetBestRatedProductsCommand)
    : EitherT[Future, String, List[RatedProduct]] = {
    val path = Path("/" + Review.index) / "_search"
    val payload = ProductQueries.getBestRated(
      command.copy(
        limit = scala.math.max(command.limit, configuration.maxResponseLength)))
    val queryParams = Query("size" -> "0")

    for {
      response <- EitherT.right[String](
        httpService
          .makeRequest(baseUri, HttpMethods.GET, path, payload, queryParams))
      entity <- EitherT(httpService.extractEntity(response))
      products <- EitherT.right[String](
        Unmarshal(entity).to[BucketAggregationResult[RatedProduct]])
    } yield {
      products.data
    }
  }

  def getBestRatedSource(command: GetBestRatedProductsCommand)
    : Source[StreamedRatedProduct, NotUsed] = {
    sealed trait BatchState
    case object Starting extends BatchState
    case class Running(afterKey: String) extends BatchState
    case object Finished extends BatchState

    val path = Path("/" + Review.index) / "_search"
    val queryParams = Query("size" -> "0")

    def getBatch(afterKey: Option[String])
      : Future[Option[(BatchState, List[StreamedRatedProduct])]] = {
      val payload = ProductQueries.getBestRatedStream(command.start,
                                                      command.end,
                                                      configuration.streamBatch,
                                                      afterKey)

      val action = for {
        response <- EitherT.right[String](
          httpService
            .makeRequest(baseUri, HttpMethods.GET, path, payload, queryParams))
        entity <- EitherT(httpService.extractEntity(response))
        aggregation <- EitherT.right[String](
          Unmarshal(entity)
            .to[CompositeAggregationResult[StreamedRatedProduct]])
      } yield {
        aggregation
      }

      action.value.map {
        case Right(CompositeAggregationResult(data, Some(key))) =>
          Some(Running(key), data)
        case Right(CompositeAggregationResult(data, None)) =>
          Some(Finished, data)
        case Left(_) =>
          None
      }
    }

    Source
      .unfoldAsync[BatchState, List[StreamedRatedProduct]](Starting) {
        case Finished     => Future.successful(None)
        case Starting     => getBatch(None)
        case Running(key) => getBatch(Some(key))
      }
      .mapConcat(_.filter(_.count >= command.min_number_reviews))
  }
}
