package com.hivemind.product

import com.hivemind.command.GetBestRatedProductsCommand
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

object ProductQueries {
  // can throw exception
  val dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd")

  def getBestRated(command: GetBestRatedProductsCommand): String =
    s"""
     |{
     |    "query": {
     |        "bool": {
     |            "filter": {
     |                "range": {
     |                    "reviewTime": {
     |                        "gte": "${dateTimeFormatter.print(command.start)}",
     |                        "lte": "${dateTimeFormatter.print(command.end)}"
     |                    }
     |                }
     |            }
     |        }
     |    },
     |    "aggs": {
     |        "data": {
     |            "terms": {
     |                "field": "asin",
     |                "size": ${command.limit},
     |                "min_doc_count": ${command.min_number_reviews},
     |                "order": {
     |                    "avg_overall": "desc"
     |                }
     |            },
     |            "aggs": {
     |                "avg_overall": {
     |                    "avg": {
     |                        "field": "overall"
     |                    }
     |                }
     |            }
     |        }
     |    }
     |}
   """.stripMargin

  def getBestRatedStream(startDate: DateTime,
                         endDate: DateTime,
                         size: Int,
                         after: Option[String]): String = {
    val afterString =
      after.map(value => s""", "after": {"data": "$value"}""").getOrElse("")

    s"""
       |{
       |    "query": {
       |        "bool": {
       |            "filter": {
       |                "range": {
       |                    "reviewTime": {
       |                        "gte": "${dateTimeFormatter.print(startDate)}",
       |                        "lte": "${dateTimeFormatter.print(endDate)}"
       |                    }
       |                }
       |            }
       |        }
       |    },
       |    "aggs": {
       |        "data": {
       |            "composite": {
       |                "size": $size,
       |                "sources": [
       |                    {
       |                        "data": {
       |                            "terms": {
       |                                "field": "asin"
       |                            }
       |                        }
       |                    }
       |                ]
       |                ${afterString}
       |            },
       |            "aggregations": {
       |                "avg_overall": {
       |                    "avg": {
       |                        "field": "overall"
       |                    }
       |                }
       |            }
       |        }
       |    }
       |}
     """.stripMargin
  }
}
