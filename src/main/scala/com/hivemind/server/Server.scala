package com.hivemind.server

import akka.NotUsed
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.sse.ServerSentEvent
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import com.hivemind.Main.system
import com.hivemind.command.{
  GetBestRatedProductsCommand,
  GetInfluencerReviewsCommand
}
import com.hivemind.domain.{InfluencerReview, StreamedRatedProduct}
import com.hivemind.product.ProductService
import com.hivemind.review.ReviewService
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport._

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}

class Server(productService: ProductService, reviewService: ReviewService)(
    implicit val materializer: ActorMaterializer) {

  import akka.http.scaladsl.marshalling.sse.EventStreamMarshalling._

  private implicit val executionContext = materializer.system.dispatcher

  private lazy val routes = path("amazon" / "best-rated") {
    post {
      decodeRequest {
        entity(as[GetBestRatedProductsCommand]) { command =>
          onComplete {
            productService
              .getBestRated(command)
              .value
          } {
            case Success(Right(products)) =>
              complete(products)
            case Success(Left(error)) =>
              complete(HttpResponse(StatusCodes.BadRequest, entity = error))
            case Failure(exception) =>
              complete((StatusCodes.InternalServerError,
                        s"An error occurred: ${exception.getMessage}"))
          }
        }
      }
    }
  } ~
    path("amazon" / "best-rated" / "stream") {
      post {
        decodeRequest {
          entity(as[GetBestRatedProductsCommand]) { command =>
            complete {
              productService
                .getBestRatedSource(command)
                .map { product =>
                  ServerSentEvent(
                    StreamedRatedProduct.jsonWrites
                      .writes(product)
                      .toString())
                }
            }
          }
        }
      }
    } ~
    path("amazon" / "influencers" / "stream") {
      post {
        decodeRequest {
          entity(as[GetInfluencerReviewsCommand]) { command =>
            complete {
              reviewService
                .getInfluencerReviewsSoruce(command)
                .map { review =>
                  ServerSentEvent(
                    InfluencerReview.format
                      .writes(review)
                      .toString())
                }

            }
          }
        }
      }
    }

  def start(): Future[Http.ServerBinding] = {
    val serverBinding: Future[Http.ServerBinding] =
      Http().bindAndHandle(routes, "0.0.0.0", 8080)

    serverBinding.onComplete {
      case Success(bound) =>
        println(
          s"Server online at http://${bound.localAddress.getHostString}:${bound.localAddress.getPort}/")
      case Failure(e) =>
        Console.err.println(s"Server could not start!")
        e.printStackTrace()
        materializer.system.terminate()
    }

    serverBinding
  }
}
